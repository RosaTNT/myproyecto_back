//Dejando el servidor arrancado y funcionando
var express = require('express');
var app = express();

var router = new express.Router();
var logger = require('./logger').Logger;


//añadimos estas lineas para  que funcione el body como entrada de Parametros
var  bodyParser = require ('body-parser');
app.use(bodyParser.json());
var port = process.env.PORT || 3000;

//incluimos dos  variables, una para almacenar la base de las peticiones de la API,
//y otra para la apikey
var baseMlabURL = "https://api.mlab.com/api/1/databases/apitechurcc/collections/";
var mLabApiKey = "apiKey=HcSWv7y0QDGPXNHy1dUk9wHhcz9kpv4_";

//incluimos variable para llamar a la aplicación externa que realiza cambio de divisa
var baseExtURL = "http://api.exchangeratelab.com/api/single/"
var mExtApiKey = "apikey=E41418C49C004CD0AA80B1A514554280";

//necesitamos un cliente para poder instanciar e iniciar una petición la exterior.
var requestJson = require('request-json');

app.use('/', router);
app.listen(port);

console.log("API escuchando en el puerto BIP BIP BIP " + port);
logger.debug("escuchando");

// preparamos una ruta de bienvenida nuestra apircctechu
app.get('/apircc/v1',
    function(req, res){
      console.log("GET /apircc/v1");
      logger.debug("GET /apircc/v1");
//devolvemos una respuesta. en formato json
      res.send(
        { "msg": "Bienvenido nuestra nueva aplicación"
        }
    );
    }
)

//Accedemos para recuperar a nuestros clientes
app.get('/apircc/v1/users',
  function(req, res){
     console.log("GET /apircc/v1/users");
//creamos un cliente que llame a la base de datos
      httpClient = requestJson.createClient(baseMlabURL);
      logger.debug("Cliente creado");

     //hacemos un get
        httpClient.get("user?" + mLabApiKey,
        function(err, resMlab, body){
          var response = !err ? body : {
            "msg" : "Error obteniendo usuarios."
          }
          res.send(response);
        }
      )
   }
)
//Accedemos para consumir apiexterna
app.get('/apircc/v1/conversor/:divisa',
  function(req, res){
     console.log("GET /apircc/v1/conversor");
//creamos un cliente que llame a la base de datos
      httpClient = requestJson.createClient(baseExtURL);
//      console.log("Cliente apiext creado");
      logger.debug("Cliente apiext creado"+ req.params.divisa);
     //hacemos un get

        httpClient.get( req.params.divisa +'?' + mExtApiKey,
        function(err, resApi, body){
          console.log(body);
          logger.debug(body);
          if (err) {
            logger.debug(err);
            response ={"msg" : "Error ejecuar apiext."
          }
        }else {
          response=body;
        }
          res.send(response);

        }
      )
   }
)

//Recuperamos la información de un cliente en concreto
app.get("/apircc/v1/users/:id",
  function(req, res){
     console.log("GET /apircc/v1/users/:id");

//creamos la variable para recuperar la id y dibujamos la Query
     var id =  req.params.id;
     var query = 'q={"id" :  ' + id + '}';
//creamos un cliente que llame a la base de datos
      httpClient = requestJson.createClient(baseMlabURL);
      logger.debug("Cliente creado");

     //hacemos un get
        httpClient.get("user?" + query + '&' + mLabApiKey,
        function(err, resMLab, body) {
//realizamos gestion de errores

           if (err) {
             logger.debug("Error obteniendo usuario");
              response = {
                      "msg" : "Error obteniendo usuario."
              }
              res.status(500);
            } else {
               if (body.length > 0) {
                        response = body[0];
             } else {
               logger.debug("Usuario no encontrado");
                    response = {
                        "msg" : "Usuario no encontrado."
               };
              res.status(404);
       }
     }
     res.send(response);
   }
      )
   }
)

// Recuperar las cuentas de un cliente
app.get("/apircc/v1/users/:id/accounts",
  function(req, res){
     console.log("GET /apircc/v1/users/:id/accounts");

//creamos la variable para recuperar la id y dibujamos la Query
     var id =  req.params.id;
     var query = 'q={"usuarioID" :  ' + id + '}';
//creamos un cliente que llame a la base de datos
      httpClient = requestJson.createClient(baseMlabURL);
      console.log("busca clientes");
      logger.debug("Buscando cuentad de cliente" + id);

     //hacemos un get
        httpClient.get("account?" + query +'&' + mLabApiKey,
        function(err, resMLab, body) {
//realizamos gestion de errores

           if (err) {
              response = {
                      "msg" : "Error obteniendo cuentas"
              }
              res.status(500);
            } else {
               if (body.length > 0) {
                        response = body;
             } else {
               logger.debug("usuario sin cuentas");

                    response = {
                        "msg" : "Usuario sin cuentas"
               };
              res.status(404);
       }
     }
     res.send(response);
   }
      )
   }
)
// Recuperar los movimiento de la cuenta de un cliente, ponemos post, para pasar parametros via body
app.post("/apircc/v1/users/account/movements",
  function(req, res){
     console.log("post /apircc/v1/users/account/movements");

//creamos la variable para recuperar la cuenta y dibujamos la query
    console.log(req.body.account + " numcliente :" + req.body.numCliente);

     var query = 'q={ "numCliente": ' + req.body.numCliente +', "account": "'+ req.body.account +'"}&s={"date" : 1}';

//creamos un cliente que llame a la base de datos
      httpClient = requestJson.createClient(baseMlabURL);
      console.log("busca movimientos");

 //hacemos un get
        httpClient.get("movements?" + query +'&' + mLabApiKey,
        function(err, resMLab, body) {
//realizamos gestion de errores

           if (err) {
              response = {
                      "msg" : "Error obteniendo movimientos del usuario"
              }
              res.status(500);
            } else {
               if (body.length > 0) {
                        response = body;
             } else {
                    response = {
                        "msg" : "Usuario sin movimientos"
               };
              res.status(404);
       }
     }
     res.send(response);
   }
      )
   }
)

//hacemos login accediendo  a MongoDB
app.post("/apircc/v1/users/login",
  function(req, res){
     console.log("GET /apircc/v1/users/login");
     logger.debug("GET /apircc/v1/users/login");

     var email =  req.body.email;
     var password = req.body.password;
     var query = 'q={"email" : "' + email + '","password":"'+password + '"}';

//creamos un cliente que llame a la base de datos
      httpClient = requestJson.createClient(baseMlabURL);
      logger.debug("Cliente creado");
//hacemos un get
        httpClient.get("user?" + query + '&' + mLabApiKey,
        function(err, resMLab, body) {
//realizamos gestion de errores
           if (err) {
             logger.debug("error obtener usuario");
              response = {
                      "msg" : "Error obteniendo usuario."
              }
              res.status(500);
              } else {
               if (body.length > 0) {
                        var putBody = '{"$set":{"logged":true}}';
                        query = 'q={"id" : '+body[0].id +'}';
//actualizo la condicion//
                    httpClient.put("user?" + query + '&' + mLabApiKey,
                        JSON.parse(putBody),
                         function(errPut,resMLabPUT,bodyPUT){
                           if (errPut) {
                             logger.debug("error actualizar usuario");
                                response = {
                                      "msg" : "Error actualizar usuario."
                              }
                              res.status(500);
                            } else {
                              response = {"msg" : "Usuario loggeado",
                                           "email" : email,
                                          "idUsuario": body[0].id,
                                           "firstname":body[0].first_name
                                          }
                              }
                                 res.send(response);
                              }
                        )
               } else {
                    logger.debug("usuario no encontrado");
                    response = {
                        "msg" : "Usuario no encontrado."
               }
               res.status(404);
               res.send(response);
               }
     }
   }
     )
     logger.debug("Termina login");
   }
)

//hacemos logout accediendo  a MongoDB
app.post("/apircc/v1/users/logout",
  function(req, res){
     console.log("GET /apircc/v1/users/logout");
     logger.debug("GET /apircc/v1/users/logout");

     var email =  req.body.email;
     var query = 'q={"email" : "' + email + '"}';

//creamos un cliente que llame a la base de datos
      httpClient = requestJson.createClient(baseMlabURL);
      logger.debug("Cliente creado");
//hacemos un get
        httpClient.get("user?" + query + '&' + mLabApiKey,
        function(err, resMLab, body) {
//realizamos gestion de errores
           if (err) {
             logger.debug("Error obtener usuario");
              response = {
                      "msg" : "Error obteniendo usuario."
              }
              res.status(500);
              } else {
               if (body.length > 0) {
                       response = body[0];

                        var putBody = '{"$unset":{"logged":""}}';
                        query = 'q={"id" : '+body[0].id +'}';
//actualizo la condicion
                        httpClient.put("user?" + query + '&' + mLabApiKey,
                        JSON.parse(putBody),
                         function(errPut,resMLabPUT,bodyPUT){
                           if (errPut) {
                             logger.debug("Error actualizar usuario " + body[0].id );
                                response = {
                                      "msg" : "Error actualizar usuario."
                              }
                              res.status(500);
                            } else {
                              logger.debug("Usuario desconectado correctamente " + email );
                              response = {"msg" : "Usuario desconectado correctamente",
                                           "email" : email}
                                        }
                                   res.send(response);
                              }
                        )
               } else {
                 logger.debug("Usuario no encontrado" );
                    response = {
                        "msg" : "Usuario no encontrado."
               }
               res.status(404);
               res.send(response);
               }
     }
   }
      )
   }
)



//hacemos alta de nuevo usuario en MongoDB
app.post("/apircc/v1/users/nuevo",
  function(req, res){
     console.log("POST /apircc/v1/users/nuevo");
     logger.debug("POST /apircc/v1/users/nuevo");
//creamos un cliente que llame a la base de datos
      httpClient = requestJson.createClient(baseMlabURL);
      console.log("Cliente creado");

//nuevo, recuperamos el máximo valor de id para sumar uno
     var query ='q=&s={"id" : -1}';
     httpClient.get("user?" + query + '&' + mLabApiKey,
      function(err, resMLab, body) {
        if (err){
          logger.debug("Error obteniendo último usuario");
              response = {
                 "msg" : "Error obteniendo último usuario"
              }
              res.status(500);
            } else{
                   var newid = parseInt(body[0].id) + 1;
                   logger.debug("devuelve el máximo" + body[0].id);
                   logger.debug("nuevo usuario:" + newid);
                  var user ={
                    "id": newid,
                    "first_name":req.body.first_name,
                    "last_name":req.body.last_name,
                    "email": req.body.email,
                    "password" : req.body.password
                  }
                  httpClient.post("user?" + '&' + mLabApiKey, user,
                       function(errPut,resMLabPUT,bodyPUT){
                            if (errPut) {
                              logger.debug("Error actualizar usuario");
                                 response = {
                                     "msg" : "Error actualizar usuario."
                                     }
                                     res.status(500);
                                   } else {
                                       logger.debug("Usuario dado de alta");
                                         response = {"msg" : "Usuario dado de alta",
                                                      "user" : user}

                                         }
                                            res.send(response);
                                         }
                                   )
    }
  })
})


//hacemos alta de cuenta en MongoDB
 app.post("/apircc/v1/users/:id/accounts",
       function(req, res){
          console.log("POST /apircc/v1/users/:id/accounts");
          logger.debug("POST /apircc/v1/users/:id/accounts");
     //creamos un cliente que llame a la base de datos
           httpClient = requestJson.createClient(baseMlabURL);
           logger.debug("Cliente creado");

           var cuenta = {
             "usuarioID":req.body.usuarioID,
            "IBAN":req.body.IBAN,
            "balance":req.body.balance
            }
     //alta de la nueva cuenta
                 httpClient.post("account?" +  '&' + mLabApiKey, cuenta,
                 function(err,resPut,bodyPUT){
                       logger.debug("dentro de la funcion");
                       logger.debug(cuenta);
                        if (err) {
                          logger.debug("Error en alta de cuenta");
                           response = {
                               "msg" : "Error alta cuenta"
                               }
                               res.status(500);
                               console.log("error");
                             } else {
                               logger.debug("Cuenta dada de alta :" + cuenta);
                                   response = {"msg" : "Cuenta dado de alta",
                                                "cuenta" : cuenta}
                                   res.send(response);

                                   }
                    }
                             )
})
//hacemos baja de usuario en MongoDB
 app.delete("/apircc/v1/users/:id",
       function(req, res){
          console.log("DELETE /apircc/v1/users/:id");
          logger.debug("DELETE /apircc/v1/users/:id");
     //creamos un cliente que llame a la base de datos
           httpClient = requestJson.createClient(baseMlabURL);
           logger.debug("Cliente creado");
           var id =  req.params.id;
           var query = 'q={"id" :  ' + id + '}';

           console.log(query);
           httpClient.get("user?" + query + '&' + mLabApiKey,
           function(err, resMLab, body) {
           //realizamos gestion de errores
              if (err) {
                logger.debug("error obtener usuario");
                 response = {
                         "msg" : "Error obteniendo usuario."
                 }
                 res.status(500);
                 } else {
                  if (body.length > 0) {
                    console.log(body[0]._id.$oid);
                    console.log(body[0]);

            httpClient.delete("user/" + body[0]._id.$oid + '?' + mLabApiKey,

              function(errDEL,resMLabDEL,bodyDEL){
              logger.debug("dentro de la funcionde borrado");

              console.log(resMLabDEL.body);
              if (errDEL) {
                   logger.debug("error borrar datos usuario");
                          response = {
                              "msg" : "Error borrar Datos Usuario."
                              }

                            } else {
                              logger.debug("Usuario borrado correctamente");
                                  response = {"msg" : "Usuario borrado correctamente",
                                               "id" : req.params.id}

                                  }
                                     res.send(response);
                                  }
                          )}
                        }
   })
})
//hacemos delete de cuenta en MongoDB
app.delete("/apircc/v1/users/:id/accounts",

      function(req, res){
         console.log("DELETE /apircc/v1/users/:id/accounts");
         logger.debug("DELETE /apircc/v1/users/:id/accounts");
    //creamos un cliente que llame a la base de datos
          httpClient = requestJson.createClient(baseMlabURL);
          logger.debug("Cliente creado");

          var query = 'q={"usuarioID" : '+ req.body.numCliente +', "IBAN": "'+ req.body.account +'"}';
           console.log(query);
           httpClient.get("account?" + query + '&' + mLabApiKey,
           function(err, resMLab, body) {
           //realizamos gestion de errores
              if (err) {
                logger.debug("error obtener cuenta");
                 response = {
                         "msg" : "Error obteniendo cuenta a borrar"
                 }
                 res.status(500);
                 } else {
                  if (body.length > 0) {
                    console.log(body[0]._id.$oid);
                    console.log(body[0]);

            httpClient.delete("account/" + body[0]._id.$oid + '?' + mLabApiKey,

              function(errDEL,resMLabDEL,bodyDEL){
              logger.debug("dentro de la funcionde borrado");
              console.log(resMLabDEL.body);
              if (errDEL) {
                   logger.debug("error borrar datos cuentas");
                          response = {
                              "msg" : "Error borrar Datos cuenta."
                              }

                            } else {
                              logger.debug("Cuenta borrada correctamente");
                                  response = {"msg" : "Cuenta borrada correctamente",
                                               "id" : req.body.numCliente,
                                                "IBAN":req.body.account}

                                  }
                                     res.send(response);
                                  }
                          )}
                        }
   })
})

//hacemos ingreso en cuenta
app.post("/apircc/v1/users/accounts/ingreso",

  function(req, res){
     console.log("POST /apircc/v1/users/accounts/ingreso");
     logger.debug("POST /apircc/v1/users/accounts/ingreso");

//creamos un cliente que llame a la base de datos
      httpClient = requestJson.createClient(baseMlabURL);
      logger.debug("vamos a hacer un ingreso");

//nuevo, recuperamos el saldo de la cuenta
      var query = 'q={"usuarioID" : '+ req.body.numCliente +', "IBAN": "'+ req.body.account +'"}';
      logger.debug(query);
        httpClient.get("account?" + query + '&' + mLabApiKey,
           function(err, resMLab, body) {
//realizamos gestion de errores
           if (err) {
              logger.debug("Error al hacer ingreso.")
              response = { "msg" : "Error al hacer ingreso" }
              res.status(500);
        }else{
           if (body.length > 0) {
                   response = body[0];
                 logger.debug("saldo" + body[0].balance);
                 logger.debug("Importe de la operacion" + req.body.import);

                  body[0].balance = Number(Number.parseFloat(body[0].balance) + Number.parseFloat(req.body.import)).toFixed(2);

                  body
                  logger.debug("Actualizamos el saldo de la cuenta" + body[0].balance);
                  var putBody =  '{"$set":{"balance":' +  body[0].balance +'}}';

                   logger.debug(query);
                   httpClient.put("account?" + query + '&' + mLabApiKey,
                   JSON.parse(putBody),

                    function(errPut,resMLabPUT,bodyPUT){
                      console.log(putBody);
                      if (errPut) {
                           response = {
                                 "msg" : "Error actualizar el balance"
                          }
                         res.status(500);
                       } else {
                         //recuperamos la fecha del dia
                              var meses = new Array ("Ene","Feb","Mar","Abr","May","Jun","Jul","Agos","Sep","Oct","Nov","Dic");
                              var f = new Date();
                              var fechaHoy=f.getDate() + "-" + meses[f.getMonth()] + "-" + f.getFullYear();
                              logger.debug("fecha" + fechaHoy);
                              console.log(parseFloat(body[0].balance));
                              var movimiento ={
                                "numCliente": req.body.numCliente,
                                "account":req.body.account,
                                "type": req.body.tipo,
                                "import": req.body.import,
                                "date" : fechaHoy,
                                "saldo" : body[0].balance
                              }


                              httpClient.post("movements?" + query + '&' + mLabApiKey,
                              movimiento,
                               function(errPOST,resMLabPOST,bodyPOST){
                                 if (errPOST) {
                                      response = {
                                            "msg" : "Error actualizar movimiento"}
                                    res.status(500);
                                  } else {
                                    console.log("Ingreso realizado correctamente");
                                    logger.debug("Ingreso realizado correctamente");
                                     response = {"msg" : "Ingreso Realizado Correctamente" }
                                  }
                               }
                            )
                              res.send(response);
                         }
                       }
                   )
        }else{
          console.log("Cuenta inexistente");
          response = {"msg" : "Cuenta  destino fondos inexistente" }
          res.status(404);
          res.send(response);
          }
      }})
}
)
//hacemos reintegro en cuenta
app.post("/apircc/v1/users/accounts/reintegro",

  function(req, res){
     console.log("POST /apircc/v1/users/accounts/reintegro");
     logger.debug("POST /apircc/v1/users/accounts/reintegro");

//creamos un cliente que llame a la base de datos
      httpClient = requestJson.createClient(baseMlabURL);
      logger.debug("vamos a hacer un reintegro");

//nuevo, recuperamos el saldo de la cuenta
      var query = 'q={"usuarioID" : '+ req.body.numCliente +', "IBAN": "'+ req.body.account +'"}';
      logger.debug(query);
        httpClient.get("account?" + query + '&' + mLabApiKey,
           function(err, resMLab, body) {
//realizamos gestion de errores
           if (err) {
              logger.debug("Reintegro.Error al recuperar saldo.")
              response = { "msg" : "Error al recuperar saldo" }
              res.status(404);
        }else{
           if (body.length > 0) {
                   response = body[0];

                   logger.debug("saldo" + Number.parseFloat(body[0].balance));
                   logger.debug("importe" + req.body.importe);
                   body[0].balance = Number.parseFloat(body[0].balance - req.body.import).toFixed(2);

                  //  console.log("num2" + Number.parseFloat(body[0].balance));
                  //  console.log("num2" + Number.parseFloat(body[0].balance).toFixed(2));

                 logger.debug("Actualizamos el saldo de la cuenta" + body[0].balance);

                  //  var putBody =  '{"$set":{"balance":' +  body[0].balance +'}}';
                   var putBody =  '{"$set":{"balance":' +  body[0].balance +'}}';

                   logger.debug(query);
                   httpClient.put("account?" + query + '&' + mLabApiKey,
                   JSON.parse(putBody),

                    function(errPut,resMLabPUT,bodyPUT){
                      logger.debug(putBody);
                      if (errPut) {
                           response = {
                                 "msg" : "Reintegro.Error actualizar el balance"
                          }
                         res.status(500);
                       } else {
                         //recuperamos la fecha del dia
                              var meses = new Array ("Ene","Feb","Mar","Abr","May","Jun","Jul","Agos","Sep","Oct","Nov","Dic");
                              var f = new Date();
                              var fechaHoy=f.getDate() + "-" + meses[f.getMonth()] + "-" + f.getFullYear();
                              logger.debug("fecha" + fechaHoy);
                              var importe =  parseFloat(req.body.import) * -1;
                              var movimiento ={
                                "numCliente": req.body.numCliente,
                                "account":req.body.account,
                                "type": req.body.tipo,
                                "import": importe,
                                "date" : fechaHoy,
                                "saldo" : body[0].balance
                              }


                              httpClient.post("movements?" + query + '&' + mLabApiKey,
                              movimiento,
                               function(errPOST,resMLabPOST,bodyPOST){
                                 if (errPOST) {
                                      response = {
                                            "msg" : "Error actualizar movimiento"}
                                    res.status(500);
                                  } else {
                                    console.log("Reintegro realizado correctamente");
                                    logger.debug("Reintegro realizado correctamente");
                                     response = {"msg" : "Reintegro Realizado Correctamente" }
                                  }
                               }
                            )
                              res.send(response);
                         }
                       }
                   )
        }else{
          console.log("Cuenta inexistente");
          response = {"msg" : "Cuenta  origen fondos inexistente" }
          res.status(404);
          res.send(response);
                }
      }})
}
)

function writeLogDataToFile (data){
  console.log("a escribir en log");
  router.use(function timeLog(req, res, next) {
    // this is an example of how you would call our new logging system to log an info message
    logger.debug(data);
    next();
  });


}
