// Firstly we'll need to import the fs library
var fs = require('fs');

// next we'll want make our Logger object available
// to whatever file references it.
var Logger = exports.Logger = {};


var debugStream = fs.createWriteStream('logs/debug.txt');
console.log("en logger:" );


Logger.debug = function(msg) {
  var message = new Date().toISOString() + " : " + msg + "\n";
  debugStream.write(message);
};
